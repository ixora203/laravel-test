<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        \App\Models\User::factory()->create([
            'name' => 'John Michael',
            'email' => 'john@quiz.com',
            'password' => Hash::make('123456'),
            'position' => 'Manager'
        ]);

        \App\Models\User::factory()->create([
            'name' => 'Alexa Liras',
            'email' => 'alexa@quiz.com',
            'password' => Hash::make('123456'),
            'position' => 'Developer'
        ]);

        \App\Models\User::factory()->create([
            'name' => 'Laurent Perrier',
            'email' => 'laurent@quiz.com',
            'password' => Hash::make('123456'),
            'position' => 'Projects Manager'
        ]);
    }
}
