<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Auth;
use Illuminate\Support\Facades\Hash;

class AdminSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $data = DB::table('users')
            ->orderBy('id','desc')
            ->paginate(20);
        
        return view('/adminsetting/index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=DB::table('users')
        ->where('users.id', '=', $id)
        ->get();
      
        return view('/adminsetting/edit', compact(['data']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $lastcreate = date('Y-m-d H:i:s'); // date time update
            // validate ก่อน insert
            $request->validate([
                'name'=>'required',
                'email'=>'required',
                'position'=>'required',
            ]);
            // เพิ่มข้อมูลที่ต้องการ
            $request->request->add([
                'updated_at' => $lastcreate
            ]);
            


            $data = request()->except(['_token','_method']); //นำค่าที่ไม่จำเป็นออก

            //for update data in table
            DB::table('users')->where('id','=', $id)->update([
                'name' => $request->name,
                'email'=> $request->email,
                'position'=> $request->position,
            ]);

            return redirect('/adminsetting')->with('edit','แก้ไขข้อมูลสำเร็จ!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete=DB::table('users')->where('id', '=', $id)->delete();
        return redirect('/adminsetting');
    }
}
