@extends('app')
@section('content')
<main class="login-form">
    <div class="cotainer">
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-md-4" style="margin: 0; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
                <div class="card" style="border:0px;">
                    <h3 class="card-header text-start" style="background: -webkit-linear-gradient(#090979, #00d4ff); -webkit-background-clip: text;-webkit-text-fill-color: transparent; border:0px;">Sign in</h3>
                    <h5>&nbsp;&nbsp;&nbsp;Enter your Email and Password to Sign in.</h5>
                    <div class="card-body">
                        <form method="POST" action="{{ route('login.custom') }}">

                            @csrf

                            <div class="form-group mb-3">
                                <label for="email" class="my-2"> <b>Email: </b></label>
                                <input type="text" placeholder="Email" id="email" class="form-control" name="email" required
                                    autofocus>
                                @if ($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>

                            <div class="form-group mb-3">
                                <label for="password" class="my-2"> <b>Password: </b></label>
                                <input type="password" placeholder="Password" id="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                <span class="text-danger">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                    
                            <div class="d-grid mx-auto">
                                <button type="submit" class="btn btn-block text-white" style="background: rgb(2,0,36); background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(9,9,121,1) 0%, rgba(0,212,255,1) 100%);">Signin</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection