@extends('layouts.app')

@section('content')
<div class="container-fulid">
    <div class="row">
        <div class="col-12">
            <h2 class="text-muted">แก้ไขรหัสผ่าน</h2>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        @foreach ($data as $row)
                        <form class="" action="/adminsetting/update_password/{{$row->id}}}" method="get"
                            enctype="multipart/form-data">
                            @csrf @method('GET')

                            <input type="hidden" name="id" value="{{$row->id}}">
                            <div class="form-group">
                                <label>รหัสผ่าน <span style="color:red;">*</span></label>
                                <input id="password" type="password" class="form-control" name="password" required>
                            </div>
                            <div class="form-group">
                                <label>ยืนยันรหัสผ่าน <span style="color:red;">*</span></label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="{{route('adminsetting.index')}}" type="submit" class="btn btn-warning">Back</a>
                        </form>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection