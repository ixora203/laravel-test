@extends('app')

@section('content')
    <div class="container-fulid">
        <div class="row">
            <div class="col-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-body">
                        <h2 class="text-muted">
                            @php
                                $ex_menu = explode("/",Request::server('REQUEST_URI'));
                                echo $ex_menu[1];
                            @endphp
                        </h2>
                        <div class="table-responsive">
                            @foreach ($data as $row)
                                <form class="" action="{{ route('adminsetting.update', $row->id) }}"
                                    method="post" enctype="multipart/form-data">
                                    @csrf @method('PUT')
                                    <input type="hidden" name="id" value="{{ $row->id }}">

                                    <div class="form-group my-2">
                                        <label>ชื่อผู้ใช้ <span style="color:red;">*</span></label>
                                        <input id="name" type="text" class="form-control" name="name" value="{{ $row->name }}" required>
                                    </div>

                                    <div class="form-group my-2">
                                        <label>อีเมล์ <span style="color:red;">*</span></label>
                                        <input id="email" type="email" class="form-control" name="email" value="{{ $row->email }}" readonly>
                                    </div>

                                    <div class="form-group my-2">
                                        <label>ตำแหน่ง <span style="color:red;">*</span></label>
                                        <select name="position" id="position" class="form-control" required>
                                            <option value="">---โปรดระบุ---</option>
                                            <option value="Manager" {{  $row->position === "Manager" ? "selected" : "" }}>Manager</option>
                                            <option value="Developer" {{  $row->position === "Developer" ? "selected" : "" }}>Developer</option>
                                            <option value="Projects Manager" {{  $row->position === "Projects Manager" ? "selected" : "" }}>Projects Manager</option>
                                        </select>
                                    </div>

                                    <div class="text-end">
                                        <button type="submit" class="btn btn-secondary my-2">Submit</button>
                                        <a href="{{ route('adminsetting.index') }}" type="submit" class="btn btn-secondary my-2">Back</a>
                                    </div>
                                    
                                </form>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
