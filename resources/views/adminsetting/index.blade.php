@extends('app')

@section('content')
    @if (session('edit'))
        <div class="alert alert-secondary alert-dismissible fade show" role="alert">
            {{ session('edit') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="container-fulid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="text-muted">
                            @php
                                $ex_menu = explode("/",Request::server('REQUEST_URI'));
                                echo $ex_menu[1];
                            @endphp
                        </h2>
                        <div class="row align-items-center">
                                <div class="col-1 col-md-1 d-none d-md-block">
                                    No.
                                </div>
                                
                                <div class="col-md-2 col-5 d-none d-md-block ">
                                    Name
                                </div>
                                <div class="col-md-3 col-6 ">
                                   Email
                                </div>
                                <div class="col-md-2 col-6 ">
                                   Position
                                </div>
                                <div class="col-md-2 col-6 ">
                                    Last Log in
                                </div>
                                <div class="col-md-2 col-4   p-0 ">
                                    Tools
                                </div>
                            </div>
                            <hr>
                        <?php
                        if (isset($_GET['page']) and $_GET['page'] != '1') {
                            $i = ($_GET['page'] - 1) * 20;
                        } else {
                            $i = 0;
                        }
                        ?>
                        @foreach ($data as $key => $row)
                            <div class="row align-items-center my-2">
                                <div class="col-1 col-md-1 d-none d-md-block">
                                    {{ $key + $i + 1 }}
                                </div>
                                
                                <div class="col-md-2 col-5 d-none d-md-block ">
                                    {{ $row->name }}
                                </div>
                                <div class="col-md-3 col-6 ">
                                    {{ $row->email }}
                                </div>
                                <div class="col-md-2 col-6 ">
                                    {{ $row->position }}
                                </div>
                                <div class="col-md-2 col-6 ">
                                    @php
                                        $ex_calendar = explode(" ", $row->email_verified_at );
                                        $ex_date = explode("-", $ex_calendar[0]);
                                        $new_date = $ex_date[2]."/".$ex_date[1]."/".$ex_date[0]." ".$ex_calendar[1];
                                    @endphp
                                    <span class="text-white px-1 py-1" style="background: rgb(2,0,36); background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(21,121,9,1) 0%, rgba(198,255,0,1) 100%); border-radius:8%;">{{ $new_date }}</span>
                                </div>
                                <div class="col-md-2 col-4   p-0 ">
                                    <form action="{{ route('adminsetting.destroy', $row->id) }}" method="post">
                                        <a href="{{ route('adminsetting.edit', $row->id) }}" class="btn btn-sm btn-secondary" title="แก้ไข"> Edit </a>
                                        @csrf @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-secondary">Delete</button>
                                    </form>
                                </div>
                            </div>

                        @endforeach


                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ $data->links() }}
@endsection
