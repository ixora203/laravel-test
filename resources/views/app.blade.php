<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Back-End</title>


    <!-- Scripts -->
    <script src="{{ asset('js/soft-ui-dashboard.js') }}" defer></script>
    <script src="{{ asset('js/soft-ui-dashboard.min.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Edu+TAS+Beginner&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/249768be2c.js" crossorigin="anonymous"></script>

    <!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>


    <!-- Styles -->
    <link href="{{ asset('css/soft-ui-dashboard.css') }}" rel="stylesheet">
    <link href="{{ asset('css/nucleo-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/nucleo-svg.css') }}" rel="stylesheet">
    <link href="{{ asset('css/soft-ui-dashboard.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

    <style>
        @import url('https://fonts.googleapis.com/css2?family=Edu+TAS+Beginner&family=Niramit:wght@200&display=swap');
        body {
            font-family: 'Niramit', sans-serif;
            background: #fafafa;
        }

        p {
            font-family: 'Niramit', sans-serif;
            font-size: 1.1em;
            font-weight: 300;
            line-height: 1.7em;
            color: #999;
        }

        a,
        a:hover,
        a:focus {
            color: inherit;
            text-decoration: none;
            transition: all 0.3s;
        }

        .navbar {
            padding: 15px 10px;
            background: #fff;
            border: none;
            border-radius: 0;
            margin-bottom: 40px;
            box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
        }

        .navbar-btn {
            box-shadow: none;
            outline: none !important;
            border: none;
        }

        .line {
            width: 100%;
            height: 1px;
            border-bottom: 1px dashed #ddd;
            margin: 40px 0;
        }

        /* ---------------------------------------------------
            SIDEBAR STYLE
        ----------------------------------------------------- */

        .wrapper {
            display: flex;
            width: 100%;
            align-items: stretch;
        }

        #sidebar {
            min-width: 300px;
            max-width: 300px;
            background: #97984B;
            color: #fff;
            transition: all 0.3s;
        }

        #sidebar.active {
            margin-left: -300px;
        }

        #sidebar .sidebar-header {
            padding: 20px;
            background: #97984B;
        }

        #sidebar ul.components {
            padding: 20px 0;
            border-bottom: 1px solid #97984B;
        }

        #sidebar ul p {
            color: #fff;
            padding: 10px;
        }

        #sidebar ul li a {
            padding: 10px;
            font-size: 1.1em;
            display: block;
        }

        #sidebar ul li a:hover {
            color: #97984B;
            background: #fff;
        }

        #sidebar ul li.active>a,
        a[aria-expanded="true"] {
            color: #fff;
            background: #97984B;
        }

        a[data-toggle="collapse"] {
            position: relative;
        }

        .dropdown-toggle::after {
            display: block;
            position: absolute;
            top: 50%;
            right: 20px;
            transform: translateY(-50%);
        }

        ul ul a {
            font-size: 0.9em !important;
            padding-left: 30px !important;
            background: #97984B;
        }

        ul.CTAs {
            padding: 20px;
        }

        ul.CTAs a {
            text-align: center;
            font-size: 0.9em !important;
            display: block;
            border-radius: 5px;
            margin-bottom: 5px;
        }

        #content {
            width: 100%;
            padding: 20px;
            min-height: 100vh;
            transition: all 0.3s;
        }

    </style>
    

</head>

<body>
    <div id="app">

        <div class="wrapper">
            <!-- Sidebar  -->
            @guest
            @else
            <nav id="sidebar" style="background-color:#5C5C5C;">
                <div class="sidebar-header" style="background-color:#5C5C5C;">
                    <h3><img src="{{ url('image/logo.png') }}" alt="logo" srcset=""  style="height:50px; width: auto;">&nbsp;QUIZ
                    </h3>
                </div>
                <hr>
                <ul class="list-unstyled components" style="background-color:#5C5C5C;">
                   
                        <li>
                            <a href="{{ url('/dashboard') }}">
                                <i class="fa-solid fa-house"></i> Dashboard
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('adminsetting.index') }}">
                                <i class="fa-solid fa-user"></i> Manage User
                            </a>
                        </li>

                        <li>
                            Account Page
                            <a href="{{ route('signout') }}">
                                <i class="fa-solid fa-right-from-bracket"></i> Logout
                            </a>
                        </li>
                    
                
                    
                </ul>

            </nav>
            @endguest

            <!-- Page Content  -->
            <div id="content">
                @guest
                @else
                <nav class="navbar navbar-expand-lg mb-3" style="opacity: 0.8;">
                    <div class="container-fluid">

                        <div class="col-6">
                            <span style="color:grey;">Page</span><span style="black">{{ Request::server('REQUEST_URI')  }}</span><br>
                            <b>
                                @php
                                    $ex_menu = explode("/",Request::server('REQUEST_URI'));
                                    echo $ex_menu[1];
                                @endphp
                            </b>
                        </div>
                        
                        <ul class="navbar-nav mr-auto" >
                            <i class="fa-solid fa-user"></i>&nbsp;&nbsp;
                            {{ Auth::user()->name }} 
                            &nbsp;&nbsp;<i class="fa-solid fa-caret-down"></i></ul>

                    </div>
                </nav>
                @endguest

                <main>
                    @yield('content')
                </main>
            </div>
        </div>

    </div>

    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous">
    </script>
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    </script>
</body>

</html>